﻿using Microsoft.CodeAnalysis;

namespace TolltechBilling
{
    public class CompiledProject
    {
        public CompiledProject(Project project, Compilation compilation)
        {
            Project = project;
            Compilation = compilation;
        }

        public Project Project { get; }
        public Compilation Compilation { get; }
    }
}