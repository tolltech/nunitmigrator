﻿using System;
using log4net;

namespace TolltechBilling
{
    public static class LogExtensions
    {
        public static void ToConsole(this ILog log, string msg)
        {
            log.Info(msg);
            Console.WriteLine(msg);
        }
    }
}