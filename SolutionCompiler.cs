﻿using System.Collections.Generic;
using System.Linq;
using log4net;
using Microsoft.CodeAnalysis.MSBuild;

namespace TolltechBilling
{
    public class SolutionCompiler : ISolutionCompiler
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SolutionCompiler));

        public CompiledSolution CompileSolution(string solutionPath)
        {
            var msWorkspace = MSBuildWorkspace.Create();

            var solution = msWorkspace.OpenSolutionAsync(solutionPath).Result;

            var compiledProjectsList = new List<CompiledProject>();
            var solutionProjects = solution.Projects.ToArray();

            log.ToConsole($"Start compiling projects for {solutionPath}");

            var total = solutionProjects.Length;
            var iterator = 0;

            foreach (var project in solutionProjects)
            {
                log.ToConsole($"Start compiling project {project.Name}");

                if (project.SupportsCompilation)
                {

                    var compilation = project.GetCompilationAsync().Result;

                    compiledProjectsList.Add(new CompiledProject(project, compilation));
                }

                log.ToConsole($"Finish compiling project {project.Name}");
                log.ToConsole($"Compiling projects {++iterator}/{total}");
            }

            log.ToConsole($"Finish compiling projects for {solutionPath}");

            return new CompiledSolution
            {
                CompiledProjects = compiledProjectsList.ToArray(),
                Solution = solution
            };
        }
    }
}