﻿namespace TolltechBilling
{
    public interface ISolutionCompiler
    {
        CompiledSolution CompileSolution(string solutionPath);
    }
}