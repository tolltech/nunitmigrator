﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Editing;

namespace TolltechBilling
{
    public interface IFixer
    {
        int Order { get; }
        void Fix(Document document, DocumentEditor documentEditor);
    }
}