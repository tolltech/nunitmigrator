﻿using System;
using System.Linq;
using log4net;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Editing;
using Microsoft.CodeAnalysis.MSBuild;
using Ninject;

namespace TolltechBilling
{
    class Program
    {
        private static ILog log = null;

        private const string testSolutionPath = "../../TestCase/TestCases.sln";

        static void Main(string[] args)
        {
            try
            {
                var standardKernel = new StandardKernel(new ConfigurationModule("log4net.config"));

                log = LogManager.GetLogger(typeof(Program));

                var solutionCompiler = standardKernel.Get<ISolutionCompiler>();

                var msWorkspace = MSBuildWorkspace.Create();

                var solution = msWorkspace.OpenSolutionAsync(args.Length > 0 ? args[0] : testSolutionPath).ConfigureAwait(false).GetAwaiter().GetResult();
                //var solution = msWorkspace.OpenSolutionAsync(testSolutionPath).Result;

                var fixers = standardKernel.GetAll<IFixer>().OrderBy(x => x.Order).ToArray();

                var f = 0;
                foreach (var fixer in fixers)
                {
                    Process(fixer, ref solution, ++f, fixers.Length);
                }                

                var success = solution.Workspace.TryApplyChanges(solution);
            }
            catch (Exception ex)
            {
                if (log == null)
                    throw;

                log.Error($"Something goes wrong", ex);
                Console.WriteLine(ex.Message);
            }
        }

        private static void Process(IFixer fixer, ref Solution solution, int f, int totalF)
        {
            var projectIds = solution.Projects.Where(x => x.Name.Contains("Test")).Select(x => x.Id).ToArray();
            var p = 0;
            foreach (var projectId in projectIds)
            {
                ++p;


                var currentProject = solution.GetProject(projectId);
                log.ToConsole($"Project {currentProject.Name}");
                var d = 0;
                var documentIds = currentProject.Documents.Select(x=>x.Id).ToArray();
                foreach (var documentId in documentIds)
                {
                    ++d;


                    var document = currentProject.GetDocument(documentId);
                    log.ToConsole($"{f:00}/{totalF} - {p:00}/{projectIds.Length} - {d:0000}/{documentIds.Length} // {currentProject.Name} // {document.Name}");
                    if (!document.SupportsSyntaxTree)
                        continue;

                    var documentEditor = DocumentEditor.CreateAsync(document).Result;

                    fixer.Fix(document, documentEditor);

                    var newDocument = documentEditor.GetChangedDocument();
                    var newProject = newDocument.Project;
                    currentProject = newProject;
                }

                solution = currentProject.Solution;
            }
        }        
    }
}
