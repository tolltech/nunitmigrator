﻿using System;
using NUnit.Framework;
using Rhino.Mocks;

namespace TestCase
{
    public class ExpectedExceptionTestCase
    {
        protected override void SetUp()
        {
            DBTestHelpers.TruncateKeyspace(container, "Errors");
        }

        private void ClearTable<T>()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void TestAddServicullResultContractIsInUsing2(int t)
        {
            using (new MockRepository().Record())
            {
                var i = 1;
            }

            var actual = "asdasd";

            using (var d = new MockRepository().Record())
            {
                d.Expect(x => x.Equals(1)).Return(1);
                Assert.That(actual, DataContractIs.EqualTo(42));
                var o = 0;
                Assert.That(actual, DataContractIs.EqualTo(43));
            }
        }

        [Test]
        public void TestAddServicullResultContractManyTestCases1(int t)
        {
            using (new MockRepository().Record())
            {
                var i = 1;
            }

            var actual = "asdasd";

            for (int i = 0; i < 2; ++i)
            {
                Assert.That((int)actual, DataContractIs.EqualTo(new Cord));
            }
        }


        [Test]
        public void TestAddServicullResultContractIsInUsing(int t)
        {
            using (new MockRepository().Record())
            {
                var i = 1;
            }

            var actual = "asdasd";

            using (new MockRepository().Record())
            {
                Assert.That(actual, DataContractIs.EqualTo(new Cord));
            }
        }


        [Test]
        [TestCase(2, Result = 2)]
        [TestCase(22, Result = 22)]
        [TestCase(23, Result = 222)]
        public void TestAddServicullResultContractManyTestCases(int t)
        {
            using (new MockRepository().Record())
            {
                var i = 1;
            }

            var actual = "asdasd";

            for (int i = 0; i < 2; ++i)
            {
                Assert.That(actual, DataContractIs.EqualTo(new Cord));
            }
        }

        [Test]
        [TestCase(2, Result = 2)]
        public void TestAddServicullResult234(int t)
        {
            using (new MockRepository().Record())
            {
                var i = 1;
            }

            var actual = "asdasd";

            Assert.IsNotNullOrEmpty(actual);

            Assert.IsNullOrEmpty(actual);
        }


        [Test]
        [TestCase(2, Result = 2)]
        public void TestAddServicullResultContractIs(int t)
        {
            using (new MockRepository().Record())
            {
                var i = 1;
            }

            var actual = "asdasd";

            Assert.That(actual, DataContractIs.EqualTo(new Cord));

            Assert.That(actual, DataContractIs.EqualTo(4545));

            Assert.That(actual, DataContractIs.EqualTo(i));
        }

        [Test, ExpectedException(typeof(InvalidOperationException))]
        public void TestAddServicePlanToTariffPlanWhenExists()
        {
            using (new MockRepository().Record())
            {
                var i = 1;
            }

            new Worker().Work(42);
        }

        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void TestAddServicePlanToTariffPlanWhenExistsNull()
        {
            using (new MockRepository().Record())
            {
                var i = 1;
            }

            new Worker().Work(42);
        }

        [Test]
        [TestCase(2, Result = 2)]
        public int TestAddServicullResult(int t)
        {
            using (new MockRepository().Record())
            {
                var i = 1;
            }

            return new Worker().Work(t);
        }
    }
}