﻿using Microsoft.CodeAnalysis;

namespace TolltechBilling
{
    public class CompiledSolution
    {
        public CompiledProject[] CompiledProjects { get; set; }
        public Solution Solution { get; set; }
    }
}